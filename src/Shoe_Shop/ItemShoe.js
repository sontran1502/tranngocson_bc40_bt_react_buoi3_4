import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    // console.log(this.props);
    return (
      <div className='col-md-3 p-4 '>
        <div className=" card" >
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h3 className="card-text ">Giá: {price}</h3>
            <a href="#"
              onClick={() => { this.props.handleAddToCart(this.props.data) }}
              className="btn btn-primary mb-3">Add To Card</a>
          </div>
        </div>
      </div>
    )
  }
}
