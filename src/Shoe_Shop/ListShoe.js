import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
  renderItem= () => { 
    return (this.props.ListShoe.map((item) => { 
      return <ItemShoe handleAddToCart = {this.props.handleAddToCart} key={item.name} data ={item}/>
     }))
   }
  render() {
   
    return (
      <div className='row'>{this.renderItem()}</div>
    )
  }
}
