import React, { Component } from 'react'
import Cart from './Cart'
import ListShoe from './ListShoe'
import { dataShoe } from './Data'

export default class Shoe_shop extends Component {
  state = {
    ListShoe: dataShoe,
    cart: [],
  }
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id
    })

    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 }
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong += 1;
    }
    this.setState({
      cart: cloneCart,
    })
  }
  handleChangeQuantity = (id, isCheck) => {
    // soLuong: 1 hoặc -1
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => { return item.id == id })
    if(cloneCart[index].soLuong==1&&isCheck==1){
      cloneCart[index].soLuong+=isCheck;
    }else if(cloneCart[index].soLuong==1 && isCheck==-1){
      cloneCart.splice(index,1)
    }else if(cloneCart[index].soLuong>1){
      cloneCart[index].soLuong+=isCheck;
    }else{
      cloneCart.splice(index,1)
    }
    this.setState({
      cart: cloneCart,
    })
  }
  handleRemote = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => { return item.id == id })
    cloneCart.splice(index,1)
    this.setState({
      cart: cloneCart,
    })
  }
  render() {
    return (
      <div>
        <h2 className='tex-center'>Shoe_shop</h2>
        <Cart handleRemote={this.handleRemote} handleChangeQuantity={this.handleChangeQuantity} cart={this.state.cart} />
        <ListShoe handleAddToCart={this.handleAddToCart} ListShoe={this.state.ListShoe} />
      </div>
    )
  }
}
