import logo from './logo.svg';
import './App.css';
import Shoe_shop from './Shoe_Shop/Shoe_shop';

function App() {
  return (
    <div className="App">
      <Shoe_shop/>
    </div>
  );
}

export default App;
